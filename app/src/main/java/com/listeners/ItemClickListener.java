package com.listeners;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

public class ItemClickListener implements View.OnClickListener {

    private int position;
    private Context context;

    public ItemClickListener(Context context, int position) {
        this.position = position;
        this.context = context;
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(context, "Clicked Item Position: " + position, Toast.LENGTH_SHORT).show();
    }
}
