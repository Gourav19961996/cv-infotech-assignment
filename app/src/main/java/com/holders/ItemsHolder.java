package com.holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cvinfotechassignment.R;

public class ItemsHolder extends RecyclerView.ViewHolder {

    public ImageView ivUserImage;
    public TextView tvUserName, tvUserAddress;
    public LinearLayout llItemWrapper;

    public ItemsHolder(@NonNull View itemView) {
        super(itemView);

        ivUserImage = itemView.findViewById(R.id.ivUserImage);
        tvUserName = itemView.findViewById(R.id.tvUserName);
        tvUserAddress = itemView.findViewById(R.id.tvUserAddress);
        llItemWrapper = itemView.findViewById(R.id.llItemWrapper);
    }
}
