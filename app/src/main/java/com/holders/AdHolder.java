package com.holders;

import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cvinfotechassignment.R;

public class AdHolder extends RecyclerView.ViewHolder {

    public FrameLayout fl_adplaceholder;
    public AdHolder(@NonNull View itemView) {
        super(itemView);

        fl_adplaceholder = itemView.findViewById(R.id.fl_adplaceholder);

    }
}
