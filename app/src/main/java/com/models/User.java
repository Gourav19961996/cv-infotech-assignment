package com.models;

import com.google.gson.annotations.SerializedName;

public class User {
////    {
////        "name": "Gourav Dhankher",
////            "profile_pic": "",
////            "Address": "XYZ",
//                "type": "user/ad"
//
////    }


    @SerializedName("name")
    private String name;
    @SerializedName("profile_pic")
    private String profilePic;
    @SerializedName("Address")
    private String Address;
    @SerializedName("type")
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
